//
//  Result.swift
//  URLSession Extensions
//
//  Copyright © 2018 David van Enckevort. All rights reserved.
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
#if swift(>=5.0)
// Swift 5 includes a Result type
#else
/// A value that represents either a success or failure, capturing associated
/// values in both cases. Adapted from SE-0235.
public enum Result<Value, Error> {
    /// A normal result, storing a `Value`.
    case value(Value)

    /// An error result, storing an `Error`.
    case error(Error)

    /// Evaluates the given transform closure when this `Result` instance is
    /// `.success`, passing the value as a parameter.
    ///
    /// Use the `map` method with a closure that returns a non-`Result` value.
    ///
    /// - Parameter transform: A closure that takes the successful value of the
    ///   instance.
    /// - Returns: A new `Result` instance with the result of the transform, if
    ///   it was applied.
    public func map<NewValue>(
        _ transform: (Value) -> NewValue
        ) -> Result<NewValue, Error> {
        switch self {
        case .value(let value):
            return .value(transform(value))
        case .error(let error):
            return .error(error)
        }
    }

    /// Evaluates the given transform closure when this `Result` instance is
    /// `.failure`, passing the error as a parameter.
    ///
    /// Use the `mapError` method with a closure that returns a non-`Result`
    /// value.
    ///
    /// - Parameter transform: A closure that takes the failure value of the
    ///   instance.
    /// - Returns: A new `Result` instance with the result of the transform, if
    ///   it was applied.
    public func mapError<NewError>(
        _ transform: (Error) -> NewError
        ) -> Result<Value, NewError> {
        switch self {
        case .value(let value):
            return .value(value)
        case .error(let error):
            return .error(transform(error))
        }
    }

    /// Evaluates the given transform closure when this `Result` instance is
    /// `.success`, passing the value as a parameter and flattening the result.
    ///
    /// - Parameter transform: A closure that takes the successful value of the
    ///   instance.
    /// - Returns: A new `Result` instance, either from the transform or from
    ///   the previous error value.
    public func flatMap<NewValue>(
        _ transform: (Value) -> Result<NewValue, Error>
        ) -> Result<NewValue, Error> {
        switch self {
        case .value(let value):
            return transform(value)
        case .error(let error):
            return .error(error)
        }
    }

    /// Evaluates the given transform closure when this `Result` instance is
    /// `.failure`, passing the error as a parameter and flattening the result.
    ///
    /// - Parameter transform: A closure that takes the error value of the
    ///   instance.
    /// - Returns: A new `Result` instance, either from the transform or from
    ///   the previous success value.
    public func flatMapError<NewError>(
        _ transform: (Error) -> Result<Value, NewError>
        ) -> Result<Value, NewError> {
        switch self {
        case .value(let value):
            return .value(value)
        case .error(let error):
            return transform(error)
        }
    }

    /// Unwraps the `Result` into a throwing expression.
    ///
    /// - Returns: The success value, if the instance is a success.
    /// - Throws:  The error value, if the instance is a failure.
    public func unwrapped() throws -> Value {
        if case .value(let value) = self {
            return value
        } else {
            throw NSError(domain: #function, code: 1, userInfo: nil)
        }
    }
}

extension Result where Error == Swift.Error {
    /// Create an instance by capturing the output of a throwing closure.
    ///
    /// - Parameter throwing: A throwing closure to evaluate.
    @_transparent
    public init(catching body: () throws -> Value) {
        do {
            let value = try body()
            self = .value(value)
        } catch {
            self = .error(error)
        }
    }
}

extension Result : Equatable where Value : Equatable, Error : Equatable { }

extension Result : Hashable where Value : Hashable, Error : Hashable { }
#endif
