//
//  HTTPMethod.swift
//  URLSession Extensions
//
//  Copyright © 2018 David van Enckevort. All rights reserved.
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
import Foundation

/// Enumeration of the Common HTTP methods defined in the HTTP/1.1 specification.
/// See: https://www.w3.org/Protocols/rfc2616/rfc2616-sec9.html
///
/// - POST: See: https://www.w3.org/Protocols/rfc2616/rfc2616-sec9.html#sec9.5
/// - GET: See: https://www.w3.org/Protocols/rfc2616/rfc2616-sec9.html#sec9.3
/// - PUT: See: https://www.w3.org/Protocols/rfc2616/rfc2616-sec9.html#sec9.6
/// - DELETE: See: https://www.w3.org/Protocols/rfc2616/rfc2616-sec9.html#sec9.7
/// - HEAD: See: https://www.w3.org/Protocols/rfc2616/rfc2616-sec9.html#sec9.4
/// - TRACE: See: https://www.w3.org/Protocols/rfc2616/rfc2616-sec9.html#sec9.8
/// - CONNECT: See: https://www.w3.org/Protocols/rfc2616/rfc2616-sec9.html#sec9.9
/// - OPTIONS: See: https://www.w3.org/Protocols/rfc2616/rfc2616-sec9.html#sec9.2
/// - PATCH: See: https://tools.ietf.org/html/rfc5789
public enum HTTPMethod: String {
    case POST, GET, PUT, DELETE, HEAD, TRACE, CONNECT, OPTIONS, PATCH
}
