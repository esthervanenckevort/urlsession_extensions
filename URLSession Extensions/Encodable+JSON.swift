//
//  Encodable+JSON.swift
//  URLSession Extensions
//
//  Created by David van Enckevort on 07/01/2019.
//  Copyright © 2019 All Things Digital. All rights reserved.
//

import Foundation

public extension Encodable {
    public func json(with encoder: JSONEncoder = JSONEncoder()) throws -> Data {
        return try encoder.encode(self)
    }
}
