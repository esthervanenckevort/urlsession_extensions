//
//  URLSession+Result.swift
//  URLSession Extensions
//
//  Copyright © 2018 David van Enckevort. All rights reserved.
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
import Foundation

public extension URLSession {
    public typealias DataTaskHandler = ((Result<(Data, URLResponse), Error>) -> Void)

    // MARK: - Data Task wrappers
    public func dataTask(with url: URL, completionHandler: @escaping DataTaskHandler) -> URLSessionDataTask {
        return dataTask(with: URLRequest(url: url), completionHandler: completionHandler)
    }

    public func dataTask(with request: URLRequest, completionHandler: @escaping DataTaskHandler) -> URLSessionDataTask {
        return dataTask(with: request) { (data, response, error) in
            if let error = error {
                completionHandler(.error(error))
            } else {
                guard let data = data, let response = response else {
                    preconditionFailure("Invalid state in \(#function)")
                }
                completionHandler(.value((data, response)))
            }
        }
    }

    public func dataTask<DataType: Decodable>(with url: URL, completionHandler: @escaping (Result<(DataType, URLResponse), Error>) -> Void) -> URLSessionTask {
        return dataTask(with: URLRequest(url: url), completionHandler: completionHandler)
    }

    public func dataTask<DataType: Decodable>(with request: URLRequest, completionHandler: @escaping (Result<(DataType, URLResponse), Error>) -> Void) -> URLSessionTask {
        return dataTask(with: request) { (result: Result<(Data, URLResponse), Error>) in
            let newValue = result.flatMap { (result: (Data, URLResponse)) -> Result<(DataType, URLResponse), Error> in
                do {
                    let (data, response) = result
                    let value = try DataType(from: data)
                    return .value((value, response))
                } catch let error {
                    return .error(error)
                }
            }
            completionHandler(newValue)
        }
    }

    // MARK: - Download Task wrappers
    public typealias DownloadTaskHandler = ((Result<(URL, URLResponse), Error>) -> Void)

    public func downloadTask(with url: URL, completionHandler: @escaping DownloadTaskHandler) -> URLSessionDownloadTask {
        return downloadTask(with: URLRequest(url: url), completionHandler: completionHandler)
    }

    public func downloadTask(with request: URLRequest, completionHandler: @escaping DownloadTaskHandler) -> URLSessionDownloadTask {
        return downloadTask(with: request) { (url, response, error) in
            if let error = error {
                completionHandler(.error(error))
            } else {
                guard let url = url, let response = response else {
                    preconditionFailure("Invalid state in \(#function)")
                }
                completionHandler(.value((url, response)))
            }
        }
    }

    public func downloadTask(withResumeData data: Data, completionHandler: @escaping DownloadTaskHandler) -> URLSessionDownloadTask {
        return downloadTask(withResumeData: data) { (url, response, error) in
            if let error = error {
                completionHandler(.error(error))
            } else {
                guard let url = url, let response = response else {
                    preconditionFailure("Invalid state in \(#function)")
                }
                completionHandler(.value((url, response)))
            }
        }
    }

    // MARK: - Upload Task wrappers
    public typealias UploadTaskHandler = ((Result<(Data, URLResponse), Error>) -> Void)

    public func uploadTask(with request: URLRequest, from data: Data?, completionHandler: @escaping UploadTaskHandler) -> URLSessionUploadTask {
        return uploadTask(with: request, from: data) { (data, response, error) in
            if let error = error {
                completionHandler(.error(error))
            } else {
                guard let data = data, let response = response else {
                    preconditionFailure("Invalid state in \(#function)")
                }
                completionHandler(.value((data, response)))
            }
        }
    }

    public func uploadTask(with request: URLRequest, fromFile file: URL, completionHandler: @escaping UploadTaskHandler) -> URLSessionUploadTask {
        return uploadTask(with: request, fromFile: file) { (data, response, error) in
            if let error = error {
                completionHandler(.error(error))
            } else {
                guard let data = data, let response = response else {
                    preconditionFailure("Invalid state in \(#function)")
                }
                completionHandler(.value((data, response)))
            }
        }
    }

    /// Convenience method to do a HTTP POST request of a JSON encoded data type.
    /// This method will configure a request with httpMethod = POST and
    /// Content-Type = application/json
    ///
    /// - Parameters:
    ///   - url: URL to POST to
    ///   - data: The data to post. This data must confirm to the Codeable protocol
    ///   - completionHandler: Completion Handler that will decode the resulting JSON
    /// - Returns: a URLSessionUploadTask
    /// - Throws: an exception when encoding the data fails.
    public func uploadJSONTask<UploadType: Encodable, DownloadType: Decodable>(with url: URL, from data: UploadType, decodableHandler completionHandler: @escaping (Result<(DownloadType, URLResponse), Error>) -> Void) throws -> URLSessionUploadTask {
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        return try uploadJSONTask(with: request, from: data, decodableHandler: completionHandler)
    }

    /// Convenience method to upload JSON encoded data.
    ///
    /// - Parameters:
    ///   - request: A fully configure request.
    ///   - data: The data to post. This data must confirm to the Codeable protocol
    ///   - completionHandler: Completion Handler that will receive deserialized data of a type that confirms to Decodable
    /// - Returns: a URLSessionUploadTask
    /// - Throws: an exception when encoding the data fails.
    public func uploadJSONTask<UploadType: Encodable, DownloadType: Decodable>(with request: URLRequest, from data: UploadType, decodableHandler completionHandler: @escaping (Result<(DownloadType, URLResponse), Error>) -> Void) throws -> URLSessionUploadTask {

        let data = try data.json()
        return uploadTask(with: request, from: data) { (result: Result<(Data, URLResponse), Error>) in
            let newValue = result.flatMap { (result: (Data, URLResponse)) -> Result<(DownloadType, URLResponse), Error> in
                do {
                    let (data, response) = result
                    let value = try DownloadType(from: data)
                    return .value((value, response))
                } catch let error {
                    return .error(error)
                }
            }
            completionHandler(newValue)
        }
    }

    /// Convenience method to upload JSON encoded data.
    ///
    /// - Note: You *MUST* specify the completionHandler by its label. e.g.:
    ///
    /// ```
    /// uploadJSONTask(with: request, from: data, dataHandler: handler)
    /// ```
    ///
    ///  Using a trailing closure will match on method that tries to decode the response.
    /// - Parameters:
    ///   - request: A fully configure request.
    ///   - data: The data to post. This data must confirm to the Codeable protocol
    ///   - completionHandler: A completion handler that handlers the outcome of the request.
    /// - Returns: a URLSessionUploadTask
    /// - Throws: an exception when encoding the data fails.
    public func uploadJSONTask<UploadType: Encodable>(with request: URLRequest, from data: UploadType, dataHandler completionHandler: @escaping (Result<(Data, URLResponse), Error>) -> Void) throws -> URLSessionUploadTask {

        let data = try data.json()
        return uploadTask(with: request, from: data, completionHandler: completionHandler)
    }

    // MARK: - EOF
}
