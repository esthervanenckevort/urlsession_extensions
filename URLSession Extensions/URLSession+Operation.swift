//
//  URLSession+Operations.swift
//  URLSession Extensions
//
//  Copyright © 2018 David van Enckevort. All rights reserved.
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import Foundation
import os

public extension URLSession {
    // MARK: - Data Task wrappers
    /// Operation based data Task
    ///
    /// - Parameter request: request for the resource to retrieve
    /// - Returns: an unscheduled URLSessionDataOperation instance
    public func dataTask(with request: URLRequest) -> DataTaskOperation {
        return DataTaskOperation(with: request, session: self)
    }

    public func dataTask(with url: URL) -> DataTaskOperation {
        let request = URLRequest(url: url)
        return DataTaskOperation(with: request, session: self)
    }

    /// Implementation of Operation that wraps a Data Task
    public class DataTaskOperation: TaskOperation<(Data, URLResponse)> {
        internal init(with request: URLRequest, session: URLSession) {
            super.init()
            task = session.dataTask(with: request) { (result) in
                self.result = result
            }
        }
    }

    // MARK: - Download Task wrappers
    public func downloadTask(with request: URLRequest) -> DownloadTaskOperation {
        return DownloadTaskOperation(with: request, session: self)
    }

    public func downloadTask(with url: URL) -> DownloadTaskOperation {
        let request = URLRequest(url: url)
        return DownloadTaskOperation(with: request, session: self)
    }

    public class DownloadTaskOperation: TaskOperation<(URL, URLResponse)> {
        internal init(with request: URLRequest, session: URLSession) {
            super.init()
            task = session.downloadTask(with: request) { (result) in
                self.result = result
            }
        }
    }

    // MARK: - Upload Task wrappers
    public func uploadTask(with request: URLRequest, from data: Data?) -> UploadTaskOperation {
        return UploadTaskOperation(with: request, from: data, session: self)
    }

    public func uploadTask(with request: URLRequest, from file: URL) -> UploadTaskOperation {
        return UploadTaskOperation(with: request, fromFile: file, session: self)
    }

    public class UploadTaskOperation: TaskOperation<(Data, URLResponse)> {
        internal init(with request: URLRequest, from data: Data?, session: URLSession) {
            super.init()
            task = session.uploadTask(with: request, from: data) { (result) in
                self.result = result
            }
        }

        internal init(with request: URLRequest, fromFile file: URL, session: URLSession) {
            super.init()
            task = session.uploadTask(with: request, fromFile: file) { (result) in
                self.result = result
            }
        }
    }

    // MARK: - Supporting code
    public class TaskOperation<R>: Operation, SuccessReporting {
        public internal(set) var result: Result<R, Error>? {
            didSet {
                guard let result = result else { return }
                _isSuccess = successVoter?(self) ?? true
                if case .error(let error) = result {
                    let error = error as NSError
                    if error.code == -999 && error.domain == "NSURLErrorDomain" {
                        os_log("Task %@ was cancelled.", self)
                        _isCancelled = true
                    }
                }
                os_log("Task %@ is finishing.", self)
                _isExecuting = false
                _isFinished = true
            }
        }
        private var _isSuccess: Bool?
        public var isSuccess: Bool? {
            return _isSuccess
        }
        public var successVoter: ((TaskOperation<R>) -> Bool)?
        public var shouldCancelOnDependencyFailure: Bool = false

        public override var isAsynchronous: Bool {
            return true
        }

        public override var isFinished: Bool {
            return _isFinished
        }

        public override var isExecuting: Bool {
            return _isExecuting
        }

        public override var isCancelled: Bool {
            return _isCancelled
        }

        override public func cancel() {
            task?.cancel()
        }

        private func hasFailedDependency() -> Bool {
            for dependency in dependencies {
                guard let reporting = dependency as? SuccessReporting else {
                    os_log("Task %@ dependency %@ does not reporting success status.", self, dependency)
                    continue
                }
                if reporting.isSuccess == false {
                    os_log("Task %@ dependency %@ failed.", self, dependency)
                    return true
                }
            }
            os_log("All dependencies of %@ completed successfully.", self)
            return false
        }

        override public var description: String {
            return "\(task?.originalRequest?.url?.absoluteString ?? "-") [\(task?.originalRequest?.httpMethod ?? "-")]"
        }

        override public var debugDescription: String {
            return "\(task?.originalRequest?.url?.absoluteString ?? "-") [\(task?.originalRequest?.httpMethod ?? "-")]"
        }

        override public func start() {
            os_log("Starting task %@.", self)
            if shouldCancelOnDependencyFailure && hasFailedDependency() {
                os_log("Cancelling task %@ because of failed dependencies.", self)
                cancel()
            }
            task.resume()
            _isExecuting = true
        }

        private var _isExecuting = false {
            willSet { willChangeValue(for: \.isExecuting) }
            didSet { didChangeValue(for: \.isExecuting) }
        }
        private var _isFinished = false {
            willSet { willChangeValue(for: \.isFinished) }
            didSet { didChangeValue(for: \.isFinished) }
        }
        private var _isCancelled = false {
            willSet { willChangeValue(for: \.isCancelled) }
            didSet { didChangeValue(for: \.isCancelled) }
        }

        internal var task: URLSessionTask!
    }
    // MARK: - EOF
}
