//
//  Decodable+JSON.swift
//  URLSession Extensions
//
//  Created by David van Enckevort on 07/01/2019.
//  Copyright © 2019 All Things Digital. All rights reserved.
//

import Foundation

public extension Decodable {
    public init(from data: Data, with decoder: JSONDecoder = JSONDecoder()) throws {
        self = try decoder.decode(Self.self, from: data)
    }
}
