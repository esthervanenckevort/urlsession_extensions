//
//  DecodableTests.swift
//  URLSession ExtensionsTests
//
//  Created by David van Enckevort on 07/01/2019.
//  Copyright © 2019 All Things Digital. All rights reserved.
//

import XCTest

class DecodableTests: XCTestCase {

    func testDecoding() {
        do {
            let user = try User(from: MockedData.json)
            XCTAssert(user.name == MockedData.user.name, "Expected to decode User John Doe.")
            XCTAssert(user.lastname == MockedData.user.lastname, "Expected to decode User John Doe.")
        } catch {
            XCTFail("Expected to decode a user.")
        }
    }


}
