//
//  URLSession_ExtensionsTests.swift
//  URLSession ExtensionsTests
//
//  Copyright © 2018 David van Enckevort. All rights reserved.
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import XCTest
import Mocker
import URLSession_Extensions

class URLSessionResultTests: XCTestCase {

    var session: URLSession!
    let url: URL = "https://test.example.com/"

    override func setUp() {
        let configuration = URLSessionConfiguration.ephemeral
        configuration.protocolClasses = [MockingURLProtocol.self]
        session = URLSession(configuration: configuration)
    }

    override func tearDown() {
        session.invalidateAndCancel()
    }

    func testDataTaskWithResult() {
        Mock(url: url, dataType: .html, statusCode: 200, data: [.get : MockedData.html ]).register()
        let promise = expectation(description: #function)
        let task = session.dataTask(with: url) { (result) in
            switch result {
            case .value(let (data, response)):
                XCTAssertFalse(data.isEmpty)
                XCTAssert((response as? HTTPURLResponse)?.statusCode == 200)
                promise.fulfill()
            case .error(_):
                XCTFail("Expected a successful request in \(#function)")
                promise.fulfill()
            }
        }
        task.resume()
        wait(for: [promise], timeout: 5)
    }

    func testDataTaskWithResultNoData() {
        Mock(url: url, dataType: .html, statusCode: 200, data: [.head: MockedData.noData]).register()
        let promise = expectation(description: #function)
        var request = URLRequest(url: url)
        request.httpMethod = HTTPMethod.HEAD.rawValue
        let task = session.dataTask(with: request) { (result) in
            switch result {
            case .value(let (data, response)):
                XCTAssert(data.isEmpty)
                XCTAssert((response as? HTTPURLResponse)?.statusCode == 200)
                promise.fulfill()
            case .error(_):
                XCTFail("Expected a successful request in \(#function)")
            }
        }
        task.resume()
        wait(for: [promise], timeout: 1)
    }

    func testDecodeJSON() {
        Mock(url: url, dataType: .json, statusCode: 200, data: [.get: MockedData.json]).register()
        let promise = expectation(description: #function)
        session.dataTask(with: url) { (result: Result<(User, URLResponse), Error>) in
            if case .value(let result) = result {
                let (user, _) = result
                XCTAssert(user.name == "John")
                promise.fulfill()
            } else {
                XCTFail("Expected a decoded JSON result.")
            }
        }.resume()
        wait(for: [promise], timeout: 1)
    }

    func testUploadJSON() throws {
        Mock(url: url, dataType: .json, statusCode: 200, data: [.post: MockedData.json]).register()
        let promise = expectation(description: #function)
        try session.uploadJSONTask(with: url, from: MockedData.user) { (result: Result<(User, URLResponse), Error>) in
            if case .value(let result) = result {
                let (user, _) = result
                XCTAssert(user.name == "John")
                promise.fulfill()
            } else {
                XCTFail("Expected a successful request.")
            }
        }.resume()
        wait(for: [promise], timeout: 1)
    }

    func testUploadJSONNoResponseDecoding() throws {
        Mock(url: url, dataType: .json, statusCode: 200, data: [.post: MockedData.json]).register()
        let promise = expectation(description: #function)
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        try session.uploadJSONTask(with: request, from: MockedData.user, dataHandler: { (result: Result<(Data, URLResponse), Error>) in
            if case .value(let result) = result {
                let (data, _) = result
                XCTAssert(data.count > 0)
                promise.fulfill()
            } else {
                XCTFail("Expected a successful request.")
            }
        }).resume()
        wait(for: [promise], timeout: 1)
    }
}
