//
//  MockData.swift
//  URLSession ExtensionsTests
//
//  Created by David van Enckevort on 02/01/2019.
//  Copyright © 2019 All Things Digital. All rights reserved.
//

import Foundation

public final class MockedData {
    public static let bundle = Bundle(for: MockedData.self)
    public static let html = bundle.url(forResource: "index", withExtension: "html")!.data
    public static let headers = bundle.url(forResource: "headers", withExtension: "data")!.data
    public static let json = bundle.url(forResource: "user", withExtension: "json")!.data
    public static let noData = Data()
    public static let user = User(name: "John", lastname: "Doe")
}

internal extension URL {
    /// Returns a `Data` representation of the current `URL`. Force unwrapping as it's only used for tests.
    var data: Data {
        return try! Data(contentsOf: self)
    }
}
