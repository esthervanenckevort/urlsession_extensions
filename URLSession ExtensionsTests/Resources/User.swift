//
//  User.swift
//  URLSession ExtensionsTests
//
//  Created by David van Enckevort on 02/01/2019.
//  Copyright © 2019 All Things Digital. All rights reserved.
//

import Foundation

public struct User: Codable {
    public var name: String
    public var lastname: String

    init(name: String, lastname: String) {
        self.name = name
        self.lastname = lastname
    }
}
