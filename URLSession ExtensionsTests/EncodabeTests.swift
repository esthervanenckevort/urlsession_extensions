//
//  EncodabeTests.swift
//  URLSession ExtensionsTests
//
//  Created by David van Enckevort on 07/01/2019.
//  Copyright © 2019 All Things Digital. All rights reserved.
//

import XCTest

class EncodabeTests: XCTestCase {

    func testEncoding() {
        do {
            _ = try MockedData.user.json()
        } catch {
            XCTFail("Expected to encode John Doe.")
        }
    }

    func testEncodingWithCustomEncoder() {
        do {
            let encoder = JSONEncoder()
            encoder.outputFormatting = .prettyPrinted
            let data = try MockedData.user.json(with: encoder)
            let string = String(data: data, encoding: .utf8)!
            XCTAssert(string.contains("John"))
            XCTAssert(string.contains("Doe"))
        } catch {
            XCTFail("Expected to encode John Doe.")
        }
    }

}
