//
//  URLSessionOperationTests.swift
//  URLSession ExtensionsTests
//
//  Copyright © 2018 David van Enckevort. All rights reserved.
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import XCTest
import Foundation
import Mocker
import URLSession_Extensions

class URLSessionOperationTests: XCTestCase {

    var session: URLSession!
    var queue: OperationQueue!
    let url: URL = "https://test.example.com/"

    override func setUp() {
        let configuration = URLSessionConfiguration.ephemeral
        configuration.protocolClasses = [MockingURLProtocol.self]
        session = URLSession(configuration: configuration)
        queue = OperationQueue()
    }
    override func tearDown() {
        session.invalidateAndCancel()
        queue.cancelAllOperations()
    }

    func testDataOperation() {
        Mock(url: url, dataType: .html, statusCode: 200, data: [.get : MockedData.html ]).register()
        let op: URLSession.DataTaskOperation = session.dataTask(with: url)
        queue.addOperation(op)
        queue.waitUntilAllOperationsAreFinished()
        XCTAssert(op.isFinished)
        XCTAssertNotNil(op.result)
        if case .value(let value)? = op.result {
            XCTAssert(value.0.count > 0, "Expected to have received data from \(url)")
        }
    }

    func testCancellation() {
//        Mock(url: url, dataType: .html, statusCode: 200, data: [.get : MockedData.html ]).register()
        let cancellationExpectation = expectation(description: #function)
        let op: URLSession.DataTaskOperation = session.dataTask(with: url)
        op.completionBlock = {
            if op.isCancelled == true {
                cancellationExpectation.fulfill()
            } else {
                XCTFail("Expected the operation to be cancelled")
            }
        }
        queue.addOperation(op)
        op.cancel()
        queue.waitUntilAllOperationsAreFinished()
        wait(for: [cancellationExpectation], timeout: 5)
    }

    func testDependency() {
        Mock(url: url, dataType: .html, statusCode: 200, data: [.get : MockedData.html ]).register()
        let dependencyExpectation = expectation(description: #function)
        queue.addOperation {
            dependencyExpectation.fulfill()
        }
        guard let op = queue.operations.first else {
            XCTFail("Failed to create operation in \(#function)")
            return
        }
        let dependency: URLSession.DataTaskOperation = session.dataTask(with: url)
        op.addDependency(dependency)
        queue.addOperation(dependency)
        queue.waitUntilAllOperationsAreFinished()
        wait(for: [dependencyExpectation], timeout: 5)
    }

    func testSuccessVoter() {
        Mock(url: url, dataType: .html, statusCode: 200, data: [.get : MockedData.html ]).register()
        let op: URLSession.DataTaskOperation = session.dataTask(with: url)
        op.successVoter = { task in
            return false
        }
        queue.addOperation(op)
        queue.waitUntilAllOperationsAreFinished()
        XCTAssert(op.isFinished)
        XCTAssertFalse(op.isSuccess!)
    }

    func testCancelOnDependencyFailure() {
        Mock(url: url, dataType: .html, statusCode: 200, data: [.get : MockedData.html ]).register()
        let op: URLSession.DataTaskOperation = session.dataTask(with: url)
        op.shouldCancelOnDependencyFailure = true
        let dep: URLSession.DataTaskOperation = session.dataTask(with: url)
        dep.successVoter = { task in return false }
        op.addDependency(dep)
        queue.addOperations([op, dep], waitUntilFinished: true)
        XCTAssertTrue(op.isFinished)
        XCTAssertTrue(op.isCancelled)
        XCTAssertTrue(dep.isFinished)
        XCTAssert(dep.isSuccess == false)
    }
}
